/**
 * Configuration used by Art Work management javascript functions
 * 
 * Takes the root URL/Context path so that all the auto-load javascript will function properly
 */
function AWConfig() {}
AWConfig.baseURL = "https://api.artic.edu/api/v1/artworks";

var myRes = null;

function ArtWorkIndex() {
	this.id = 0;
	this.title = "";
	this.thumbnail = null;
	this.score = 0.0;
	this.api_model = "";
	this.is_boosted = true;
	this.timestamp = "2022-09-27T23:31:52-05:00";
}

ArtWorkIndex.prototype = {		//Start Art Work Protoyping
	/**
	 * Handles the automatic loading of 'this' object from 'json' data
	 */
	fromJSON: function(jsonObj) {
		this.id = jsonObj.id;
		this.title = jsonObj.title;
		this.thumbnail = jsonObj.thumbnail;
		this.score = jsonObj._score;
		this.api_model = jsonObj.api_model;
		this.is_boosted = jsonObj.is_boosted;
		this.timestamp = jsonObj.timestamp;
	}
} //end ArtWork prototyping

//pagination":{"total":7268,"limit":10,"offset":0,"total_pages":727,"current_page":1}
function Pagination() {
	this.total = 0;
	this.limit = 0;
	this.offset = 0;
	this.total_pages = 0;
	this.current_page = 1;
}

Pagination.prototype = {
	fromJSON: function(jsonObj) {
		this.total = jsonObj.total;
		this.limit = jsonObj.limit;
		this.offset = jsonObj.offset;
		this.total_pages = jsonObj.total_pages;
		this.current_page = jsonObj.current_page;
	}
}

function ArtWorkSearchRes() {
	this.pagination = null;
	this.artworklist = null;
}

ArtWorkSearchRes.prototype = {
	fromJSON: function(jsonObj) {
		
		this.pagination = new Pagination();
		this.pagination.fromJSON(jsonObj.pagination);
		
		this.artworklist = new Array(this.pagination.limit);
		for (let i = 0; i < this.pagination.limit; i++)	{	
			var artworkIndex = new ArtWorkIndex();
			artworkIndex.fromJSON(jsonObj.data[i]);
			this.artworklist.push(artworkIndex);
		}
	}
}

ArtWorkSearchRes.viewer = function(artlist){
	$('#currentPage').val(''+artlist.pagination.current_page);
	var prev = artlist.pagination.current_page - 1;
	$('#prePage').val(''+prev);
	var next = artlist.pagination.current_page + 1;
	$('#nextPage').val(''+next);
	$('#totalPage').val(''+artlist.pagination.total_pages);
	$('#total').val(''+artlist.pagination.total);
	$('#artlist').val("");
	for (let i = 0; i < artlist.pagination.limit; i++)	{
		var temp = artlist.artworklist.pop();
		console.log(temp);
		$('#artlist').append('<li><a href="detail.html?id='+ temp.id +'">'+ temp.title +'</a></li>');
	}
}

ArtWorkSearchRes.loadSearchResult = function(searchkey, page, limit) {
	$.ajax({
		url: AWConfig.baseURL + '/search?q=' + searchkey + '&page=' + page + "&limit=" + limit,
		dataType: 'json',
		method: "GET",
		success: function(data) {
			aObj = new ArtWorkSearchRes();
			aObj.fromJSON(data);
			ArtWorkSearchRes.viewer(aObj);
		},
		error: function(){
			alert('Error occured');
		}
	});
}







/*
ArtWork.loadArtWork = function(id, success, error) {
	$.ajax({
		url: AWConfig.baseURL + '/' + id,
		dataType: 'json',
		success: function(data) {
			aObj = new ArtWork();
			aObj.fromJSON(data);
			
			if( typeof _loadedArtWork != "undefined") {
				_loadedArtWork = aObj;
			}
			
			success(aObj);
		},
		error: error
	});
}
*/



